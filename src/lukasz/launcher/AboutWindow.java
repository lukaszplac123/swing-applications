package lukasz.launcher;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import lukasz.helpers.*;

/**
 * Created by Lukasz on 2016-06-09.
 */
public class AboutWindow implements ActionListener {

    private JFrame jFrameAbout;
    private JPanel jPanelMain;
    private JPanel jPanel1;   //panel1 zawiera icone i tekst
    private JPanel jPanel2;     //panel 2 zawiera button

    AboutWindow(){
        ImageIcon img = new ImageIcon(Const.ICON_PATH+"ic-info100.png");
        jFrameAbout = new JFrame("Application Information");
        jFrameAbout.setBounds(300,300,500, 280);
        jFrameAbout.setResizable(false);
        jFrameAbout.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        jFrameAbout.setUndecorated(false);
        jFrameAbout.setAlwaysOnTop(true);
        jFrameAbout.setLayout(new BorderLayout());


        jPanelMain = new JPanel(new BorderLayout());
        jPanelMain.setBackground(Const.POPUP_WINDOW_COLOR);

        jPanel1 = new JPanel(new BorderLayout());
        jPanel1.setBorder(new EmptyBorder(20,50,20,50));
        jPanel1.setBackground(Const.POPUP_WINDOW_COLOR);

        jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBorder(new EmptyBorder(20,100,20,100));
        jPanel2.setBackground(Const.POPUP_WINDOW_COLOR);

        JLabel jLabel = new JLabel(img);

        JTextArea jTextArea = new JTextArea(Const.ABOUT_TEXT);
        jTextArea.setBorder(BorderFactory.createEmptyBorder(20,5,20,5));
        jTextArea.setBackground(Const.POPUP_WINDOW_COLOR);

        JButton jButton = new JButton("OK");
        jButton.addActionListener(this);

        jPanel1.add(jLabel, BorderLayout.WEST);
        jPanel1.add(jTextArea, BorderLayout.EAST);
        jPanel2.add(jButton, BorderLayout.CENTER);

        jPanelMain.add(jPanel1, BorderLayout.NORTH);
        jPanelMain.add(jPanel2, BorderLayout.SOUTH);

        jFrameAbout.add(jPanelMain, BorderLayout.CENTER);
        jFrameAbout.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")){
            jFrameAbout.setVisible(false);
        }
    }

}
