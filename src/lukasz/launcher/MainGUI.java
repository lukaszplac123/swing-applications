package lukasz.launcher;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import lukasz.TicTacToe.TicTacToe;
import lukasz.helpers.*;

public class MainGUI implements ActionListener{

    static private CurrentResolution resolution = new CurrentResolution(800,600);
    static private Color currentColor = Const.DEFAULT_BACKGROUND;
    static private JFrame jFrame;
    static private  JPanel jPanel;
    private MainMenu jMenuBar;
    public MainGUI(){
        jFrame = new JFrame("Application Launcher");
        jPanel = new JPanel();
        jFrame.setBounds(200,200,resolution.getX(),resolution.getY());
        jFrame.setResizable(false);
        //jFrame.getContentPane().setBackground(Const.DEFAULT_BACKGROUND);
        jPanel.setBackground(currentColor);
        jPanel.setLayout(new BorderLayout());
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLayout(new BorderLayout());

        //klasa MAinMenu - kontener menu
        jMenuBar = new MainMenu();
        jFrame.setJMenuBar(jMenuBar.getMenuBar());
        jFrame.add(jPanel);
        jFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public static void setNewRes(){
        jFrame.setSize(resolution.getX(),resolution.getY());
    }

    public static void setNewMenu(JMenuBar mm){
        jFrame.setJMenuBar(mm);
        jFrame.revalidate();
    }

    public static void setNewColor(Color col){
        currentColor = col;
        jPanel.setBackground(currentColor);
    }

    public static CurrentResolution getResolution(){
        return resolution;
    }

    public static JFrame getFrame(){
        return jFrame;
    }

    public static void startNewApp(JPanel gamePane){
        jPanel.add(gamePane);
        jPanel.setBorder(new EmptyBorder(20,20,20,20));
        jFrame.add(jPanel,BorderLayout.CENTER);
        jFrame.revalidate();
    }

    public static void stopApp (JPanel gamePane){
        jPanel.remove(gamePane);
        jFrame.repaint();
        jFrame.revalidate();
    }

}
