package lukasz.launcher;

/**
 * Created by Lukasz on 2016-06-09.
 */
public class CurrentResolution {
    private int x;
    private int y;

    CurrentResolution(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
