package lukasz.launcher;

import lukasz.helpers.Const;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Lukasz on 2016-06-09.
 */
public class ResolutionWindow implements ActionListener {
    private JRadioButton radioButton1;
    private JPanel panel1;
    private JRadioButton radioButton2;
    private JButton button1;
    private JFrame jFrame;
    private CurrentResolution resolution;

    ResolutionWindow(CurrentResolution res){
        resolution = new CurrentResolution(res.getX(),res.getY());
        jFrame = new JFrame("Resolution");
        jFrame.setLocation(300,300);
        jFrame.setResizable(false);
        jFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        jFrame.setUndecorated(false);
        jFrame.setAlwaysOnTop(true);
        jFrame.setLayout(new BorderLayout());

        panel1 = new JPanel(new BorderLayout());

        radioButton1 = new JRadioButton("800x600");
        radioButton1.setBackground(Const.POPUP_WINDOW_COLOR);
        radioButton2 = new JRadioButton("1600x1000");
        radioButton2.setBackground(Const.POPUP_WINDOW_COLOR);
        radioButton1.addActionListener(this);
        radioButton2.addActionListener(this);
        ButtonGroup bg = new ButtonGroup();
        bg.add(radioButton1);
        bg.add(radioButton2);

        button1 = new JButton("OK");
        button1.addActionListener(this);

        panel1.add(radioButton1,BorderLayout.WEST);
        panel1.add(radioButton2,BorderLayout.EAST);
        panel1.add(button1,BorderLayout.SOUTH);

        jFrame.add(panel1,BorderLayout.CENTER);

        jFrame.pack();
        jFrame.setVisible(true);

        if ((res.getX()==800) & (res.getY()==600)){
            radioButton1.setSelected(true);
        }
        else radioButton2.setSelected(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("OK")){
            jFrame.setVisible(false);
            MainGUI.getResolution().setX(resolution.getX());
            MainGUI.getResolution().setY(resolution.getY());
            MainGUI.setNewRes();
        }

        if (e.getActionCommand().equals("800x600")){
            resolution.setX(800);
            resolution.setY(600);
        }

        if (e.getActionCommand().equals("1600x1000")){
            resolution.setX(1600);
            resolution.setY(1000);
        }
    }

}
