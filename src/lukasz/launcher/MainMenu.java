package lukasz.launcher;

import lukasz.TicTacToe.TicTacToe;
import lukasz.TicTacToe.TicTacToeGame;
import lukasz.helpers.Const;
import lukasz.snake.SnakeGame;
import sun.applet.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by Lukasz on 2016-06-10.
 */
public class MainMenu implements ActionListener{

    protected JMenuBar jMenuBar = new JMenuBar();
    protected JMenu jMenuGame;
    protected TicTacToe ttt;
    protected TicTacToeMenu tttMenu;
    protected SnakeGame snake;

    private JMenuItem jMenuItemTicTacToe;
    private JMenuItem jMenuItemSnake;
    private JMenuItem jMenuItemParkman;
    private JMenuItem jMenuItemQuit;
    private JMenu jMenuSettings;
    private JMenuItem jMenuItemResolution;
    private JMenu jMenuBackground;
    private JMenuItem jMenuItemBackDefault;
    private JMenuItem jMenuItemBackCyan;
    private JMenuItem jMenuItemAbout;
    private JMenu jMenuHelp;
    private ImageIcon imageIcon = new ImageIcon(Const.ICON_PATH+"ic-info16.png");

    MainMenu() {

        //dodawanie itemow menu do kontenera menu
        ttt = new TicTacToe();
        jMenuGame = createMenuGame();
        jMenuSettings = createMenuSettings();
        jMenuHelp = createMenuHelp();
        jMenuBar.add(jMenuGame);
        jMenuBar.add(jMenuSettings);
        jMenuBar.add(jMenuHelp);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("TicTacToe")){
              tttMenu = new TicTacToeMenu();
              MainGUI.setNewMenu(tttMenu.getMenuBar());
              MainGUI.startNewApp(ttt.getPane());
        }

        if (e.getActionCommand().equals("Snake")){
              snake = new SnakeGame();
        }

        if (e.getActionCommand().equals("Exit")){
            exitCurrent();
        }

        if (e.getActionCommand().equals("Quit")){
            exitCurrent();
        }

        if (e.getActionCommand().equals("About")){
            new AboutWindow();
        }

        if (e.getActionCommand().equals("Resolution")){
            new ResolutionWindow(MainGUI.getResolution());
        }

        if (e.getActionCommand().equals("Cyan")){
            MainGUI.setNewColor(Color.CYAN);
        }
        if (e.getActionCommand().equals("Default")){
            MainGUI.setNewColor(Const.DEFAULT_BACKGROUND);
        }

        if (e.getActionCommand().equals("New Game")) {
            TicTacToeGame.closeFromMenu();

        }
    }

    public JMenuBar getMenuBar(){
        return jMenuBar;
    }

    public JMenu createMenuHelp(){
        //tworzenie menu "Help"
        jMenuHelp = new JMenu("Help");
        jMenuItemAbout = new JMenuItem("About", imageIcon);
        jMenuItemAbout.setToolTipText("Author/Version/General Information");
        jMenuItemAbout.addActionListener(this);
        jMenuHelp.add(jMenuItemAbout);
        return jMenuHelp;
    }

    public JMenu createMenuSettings() {
        //tworzenie menu "Settings"
        jMenuSettings = new JMenu("Settings");
        jMenuItemResolution = new JMenuItem("Resolution");
        jMenuItemResolution.addActionListener(this);
        jMenuBackground = new JMenu("Background");
        jMenuItemBackDefault = new JMenuItem("Default");
        jMenuItemBackDefault.addActionListener(this);
        jMenuItemBackCyan = new JMenuItem("Cyan");
        jMenuItemBackCyan.addActionListener(this);
        jMenuSettings.add(jMenuItemResolution);
        jMenuSettings.add(jMenuBackground);
        jMenuBackground.add(jMenuItemBackDefault);
        jMenuBackground.add(jMenuItemBackCyan);
        return jMenuSettings;
    }

    public JMenu createMenuGame(){
        //klasa jMenu - tworzenie menu "Game" do wyboru gry
        jMenuGame = new JMenu("Game");
        jMenuGame.setMnemonic(KeyEvent.VK_G);  //wcisniecie G otwiera menu "Game"
        jMenuItemTicTacToe = new JMenuItem("TicTacToe");
        jMenuItemTicTacToe.addActionListener(this);
        jMenuItemSnake = new JMenuItem("Snake");
        jMenuItemSnake.addActionListener(this);
        jMenuItemParkman = new JMenuItem("Packman");
        jMenuItemQuit = new JMenuItem("Quit");
        jMenuItemQuit.addActionListener(this);
        jMenuItemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK)); //kombinacja Ctr+Q wychodzi z aplikacji
        jMenuGame.add(jMenuItemTicTacToe);
        jMenuGame.add(jMenuItemSnake);
        jMenuGame.add(jMenuItemParkman);
        jMenuGame.addSeparator();
        jMenuGame.add(jMenuItemQuit);
        return jMenuGame;
    }

    protected void exitCurrent(){
        System.exit(0);
    }
}

class TicTacToeMenu extends MainMenu {

    private JMenu jMenuTicTacToe;
    private JMenuItem jMenuItemNewGame;
    private JMenuItem jMenuItemExit;

    TicTacToeMenu() {
        super();
        jMenuTicTacToe = createTicTacToeMenu();
        jMenuBar.remove(jMenuGame);
        jMenuBar.add(jMenuTicTacToe, 0);
    }

    public JMenu createTicTacToeMenu() {
        jMenuTicTacToe = new JMenu("TicTacToe");
        jMenuItemNewGame = new JMenuItem("New Game");
        jMenuItemExit = new JMenuItem("Exit");
        jMenuItemExit.addActionListener(this);
        jMenuTicTacToe.add(jMenuItemNewGame);
        jMenuItemNewGame.addActionListener(this);
        jMenuTicTacToe.addSeparator();
        jMenuTicTacToe.add(jMenuItemExit);
        return jMenuTicTacToe;
    }

    @Override
    protected void exitCurrent() {
        MainGUI.stopApp(ttt.getPane());
        MainMenu mm = new MainMenu();
        MainGUI.setNewMenu(mm.jMenuBar);
        TicTacToeGame.closeWhenExit();
    }
}
