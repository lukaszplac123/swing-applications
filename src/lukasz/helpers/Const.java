package lukasz.helpers;

import java.awt.*;



/**
 * Created by Lukasz on 2016-06-07.
 */
public class Const {
    public static final String ICON_PATH = "src/icons/";
    public static final String ABOUT_TEXT = "Author: Lukasz Plac\n" +
            "Application: Application Launcher\n" +
            "Version: 1.0\n\n" +
            "Launcher application used to start\nother applications using Menu bar.\n" +
            "Go to Games->[AppName] to choose Game ";
    public static final Color POPUP_WINDOW_COLOR = Color.LIGHT_GRAY;
    public static final Color DEFAULT_BACKGROUND = Color.WHITE;
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

}


