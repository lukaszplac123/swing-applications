package lukasz.TicTacToe;

import lukasz.helpers.Const;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;



public class TicTacToe implements ActionListener {

    public static PaintButton[] fieldButtons = new PaintButton[9];
    public static JLabel jLabelPlayer1;
    public static JLabel jLabelPlayer2;
    public static JLabel jLabelTime1;
    public static JLabel jLabelTime2;
    public static JButton startButton;
    public TicTacToeGame tttg;
    public static JPanel jPanel;

    private JPanel jPanelOptions;
    private JPanel jPanelUp;
    private JPanel jPanelDown;
    private JPanel jPanelGameField;
    private JLabel jLabelTab;
    private static JComboBox<String> jListPlayers1, jListPlayers2;
    private String[] playersList = {"Human", "Computer"};
    private JTabbedPane jTabbedPane;

    public TicTacToe(){
        jListPlayers1 = new JComboBox<String>(playersList);
        //jListPlayers1.addActionListener(this);
        jListPlayers2 = new JComboBox<String>(playersList);
        //jListPlayers2.addActionListener(this);
        jTabbedPane = new JTabbedPane();

        //jPanelOptions jest polem opcji po lewej stronie okna
        //sklada sie na niego gorny panel opcji i dolny panel opcji
        jPanelOptions = new JPanel(new BorderLayout());
        jPanelOptions.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Const.POPUP_WINDOW_COLOR,2,true),
                                                                   new EmptyBorder(0,20,0,20)));
        jPanelUp = new JPanel(new GridLayout(4,2,2,10));
        jPanelUp.setBorder(new EmptyBorder(10,0,20,0));
        jLabelTime1 = new JLabel("Time: 0s");
        jLabelTime1.setOpaque(true);
        jLabelTime1.setBackground(Color.WHITE);
        jLabelTime2 = new JLabel("Time: 0s");
        jLabelTime2.setOpaque(true);
        jLabelTime2.setBackground(Color.WHITE);
        jLabelPlayer1 = new JLabel("Cross:");
        jLabelPlayer1.setOpaque(true);
        jLabelPlayer1.setBackground(Color.WHITE);
        jLabelPlayer2 = new JLabel("Circle:");
        jLabelPlayer2.setOpaque(true);
        jLabelPlayer2.setBackground(Color.WHITE);
        startButton = new JButton("Start Game");
        startButton.addActionListener(this);
        jPanelUp.add(jLabelTime1);
        jPanelUp.add(jLabelTime2);
        jPanelUp.add(jLabelPlayer1);
        jPanelUp.add(jLabelPlayer2);
        jPanelUp.add(jListPlayers1);
        jPanelUp.add(jListPlayers2);
        jPanelUp.add(startButton);
        jPanelDown = new JPanel(new GridLayout(1,1,1,1));
        jPanelDown.setBorder(new EmptyBorder(0,0,20,0));
        jTabbedPane.addTab("Log", new LogTab());
        jTabbedPane.addTab("Chat", new ChatTab());
        jPanelDown.add(jTabbedPane);


        jPanelOptions.add(jPanelUp,BorderLayout.NORTH);
        jPanelOptions.add(jPanelDown,BorderLayout.CENTER);


        //jPanelGameField jest polem gry po prawej stronie okna
        jPanelGameField = new JPanel(new GridLayout(3,3,5,5));
        jPanelGameField.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Const.POPUP_WINDOW_COLOR,2,true),
                                                                     new EmptyBorder(100,100,100,100)));
        for (int i = 0; i < 9; i++) {
            fieldButtons[i] = new PaintButton();
            fieldButtons[i].setMargin(new Insets(35,35,35,35));
            fieldButtons[i].setBackground(Color.WHITE);
            fieldButtons[i].addActionListener(this);
            fieldButtons[i].setActionCommand(String.valueOf(i));
            fieldButtons[i].setEnabled(false);
            jPanelGameField.add(fieldButtons[i]);
        }

        jPanel = new JPanel(new BorderLayout());
        jPanel.setBorder(new EmptyBorder(5,5,5,5));

        jPanel.add(jPanelOptions, BorderLayout.WEST);
        jPanel.add(jPanelGameField, BorderLayout.EAST);
    }

    public JPanel getPane(){
        return jPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Start Game")){
            startButton.setEnabled(false);
            for (PaintButton but: fieldButtons) {
                but.clear();
                but.setEnabled(true);
            }
            tttg = new TicTacToeGame();
        }

        switch (e.getActionCommand()) {
            case "0":{
                fieldButtons[0].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "1":{
                fieldButtons[1].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "2":{
                fieldButtons[2].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "3":{
                fieldButtons[3].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "4":{
                fieldButtons[4].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "5":{
                fieldButtons[5].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "6":{
                fieldButtons[6].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "7":{
                fieldButtons[7].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            case "8":{
                fieldButtons[8].setChecked();
                TicTacToeGame.setTick();
                break;
            }
            }
        }

    public static String getPlayer1(){
        return jListPlayers1.getSelectedItem().toString();
    }
    public static String getPlayer2(){
        return jListPlayers2.getSelectedItem().toString();
    }

    public static void repaint(){
        jPanel.repaint();
        jPanel.validate();
    }

}



