package lukasz.TicTacToe;

import javax.swing.*;
import java.awt.*;

public class LogTab extends JPanel {
    public static StringBuilder log;
    private static JTextArea jTextArea;

    public LogTab(){
        setLayout(new BorderLayout());
        log = new StringBuilder();
        Color col = getBackground();
        jTextArea = new JTextArea();
        jTextArea.setBackground(col);
        JScrollPane jsp = new JScrollPane(jTextArea);

        add(jsp, BorderLayout.CENTER);
    }
    public static synchronized void setText(){
        jTextArea.setText(String.valueOf(log));
    }
}
