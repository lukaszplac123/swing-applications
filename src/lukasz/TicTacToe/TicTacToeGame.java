package lukasz.TicTacToe;

import javax.swing.*;
import java.awt.*;
import java.sql.Time;
import java.util.*;

class Player {
    private String player;
    private String label;
    private int playTime;
    private long currentTime;
    private long savedTime;


    Player(String player, String label) {
        this.player = player;
        this.label = label;
        this.playTime = 0;
        currentTime = 0;
        savedTime = System.currentTimeMillis();
    }

    public String getName() {
        return player;
    }
    public String getLabel() {
        return label;
    }


    public synchronized void move(boolean[] bool){

        if (bool[0]){
            TicTacToe.jLabelPlayer1.setBackground(Color.pink);
            TicTacToe.jLabelPlayer2.setBackground(Color.WHITE);
            LogTab.log.append("Cross moves\n");
            LogTab.setText();
        } else if (bool[1]){
            TicTacToe.jLabelPlayer1.setBackground(Color.WHITE);
            TicTacToe.jLabelPlayer2.setBackground(Color.pink);
            LogTab.log.append("Circle moves\n");
            LogTab.setText();
        }
        PaintButton.whoMoves = getLabel();
    }

    public void updateTime (boolean isActive){
        currentTime = System.currentTimeMillis();
        if (((currentTime - savedTime) > 1000) & isActive){
            playTime++;
            savedTime = currentTime;
        }

    }

    public int getTime(){
        return playTime;
    }



}

class PaintButton extends JButton{

    public static String whoMoves;

    private boolean buttonChecked;
    private String Symbol;
    private int xx,yy;

    PaintButton(){
        buttonChecked = false;
        whoMoves = "";
        Symbol = "";
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        xx = getWidth();
        yy = getHeight();
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(6));
        if (buttonChecked == true){
            if (Symbol == "Cross:") {
                g2.drawLine(20, 20, xx - 20, yy - 20);
                g2.drawLine(20, yy-20, xx - 20, 20);
            } else if (Symbol == "Circle:") {
                g2.drawArc(xx / 5, yy / 5, xx - 30, yy - 30, 0, 360);
            }
            setEnabled(false);
        }

    }

    public void setChecked(){
        buttonChecked = true;
        Symbol = whoMoves;
    }

    public String getButtonState(){
        return Symbol;
    }

    public void clear(){
        buttonChecked = false;
        Symbol = "";
    }
}

public class TicTacToeGame implements Runnable {

    private static boolean tick;
    private static boolean stopTriggerFromMenu;
    boolean isGame;
    Thread thread;

    TicTacToeGame() {
        thread = new Thread(this);
        stopTriggerFromMenu = false;
        thread.start();
        tick = false;
    }

    private boolean randomize() {
        Random rand = new Random();
        return rand.nextBoolean();
    }

    public void run() {
        Player player1 = new Player(TicTacToe.getPlayer1(), TicTacToe.jLabelPlayer1.getText());
        Player player2 = new Player(TicTacToe.getPlayer2(), TicTacToe.jLabelPlayer2.getText());
        boolean random = randomize();
        boolean[] tickBools = new boolean[2];


        //jezeli true to pierwszy ruch ma player 1
        if (randomize()) {
            tickBools[0] = true;
            tickBools[1] = false;
            player1.move(tickBools);
        } else {

            tickBools[0] = false;
            tickBools[1] = true;
            player2.move(tickBools);
        }

        isGame = true;
            while (isGame & !stopTriggerFromMenu) {
                try {
                    thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                boolean check =  checkState();
                if (tick & isGame & !check) {
                    tickBools[0] = !tickBools[0];
                    tickBools[1] = !tickBools[1];
                    //System.out.println("Tick recognized2");
                    if (tickBools[0]) {
                        player1.move(tickBools);
                    } else if (tickBools[1]) {
                        player2.move(tickBools);
                    }
                    tick = false;
                }
                if (check) {
                    if (PaintButton.whoMoves == "Circle:") {
                        LogTab.log.append("Circle won in "+player2.getTime()+ " s !!!");
                    } else LogTab.log.append("Cross won in "+player1.getTime()+ " s !!!!!!");
                    LogTab.setText();
                    stopThread();
                }
                if (noMoreTiles()) {
                    LogTab.log.append("Remis !!!");
                    LogTab.setText();
                    stopThread();
                }

                player1.updateTime(PaintButton.whoMoves.equals("Cross:"));
                player2.updateTime(PaintButton.whoMoves.equals("Circle:"));
                TicTacToe.jLabelTime1.setText("Time: "+player1.getTime()+"s");
                TicTacToe.jLabelTime2.setText("Time: "+player2.getTime()+"s");
            }
            stopThread();
        }


    private void stopThread() {
        isGame = false;
        TicTacToe.startButton.setEnabled(true);
        for (PaintButton but: TicTacToe.fieldButtons) {
            //but.clear();
            but.setEnabled(false);
        }
        TicTacToe.jLabelPlayer1.setBackground(Color.WHITE);
        TicTacToe.jLabelPlayer2.setBackground(Color.WHITE);
        int end =  LogTab.log.length();
        LogTab.log.delete(0,end);
        TicTacToe.jLabelTime1.setText("Time:0s");
        TicTacToe.jLabelTime2.setText("Time:0s");
        if (stopTriggerFromMenu) TicTacToe.repaint();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static synchronized void closeFromMenu(){
        stopTriggerFromMenu =  true;
    }
    public static synchronized void closeWhenExit(){
        stopTriggerFromMenu =  true;
    }

    private synchronized boolean checkState() {
            if (((TicTacToe.fieldButtons[0].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[1].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[2].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[3].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[5].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[6].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[7].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[0].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[3].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[6].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[1].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[7].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[2].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[5].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[0].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Cross:"))) ||
                ((TicTacToe.fieldButtons[6].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Cross:")) &&
                (TicTacToe.fieldButtons[2].getButtonState().equals("Cross:")))) {
            return (true);
        }
        if      (((TicTacToe.fieldButtons[0].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[1].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[2].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[3].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[5].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[6].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[7].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[0].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[3].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[6].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[1].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[7].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[2].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[5].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[0].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[8].getButtonState().equals("Circle:"))) ||
                ((TicTacToe.fieldButtons[6].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[4].getButtonState().equals("Circle:")) &&
                (TicTacToe.fieldButtons[2].getButtonState().equals("Circle:")))) {
            return (true);
        }
    return false;
    }

    private synchronized boolean noMoreTiles(){
        for (PaintButton but: TicTacToe.fieldButtons){
            if (but.isEnabled()) {
                    return false;
                }
            }
        return true;
        }

    public static void setTick(){
        tick = true;
        //System.out.println("Tick recognized1 "+ String.valueOf(tick));
    }

}
