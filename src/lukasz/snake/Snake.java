package lukasz.snake;


import lukasz.helpers.Const;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;


class Direction {

    public int x = 0;
    public int y = 0;
    private boolean renderLeft;
    private boolean renderRight;
    private boolean renderUp;
    private boolean renderDown;





    Direction(int x, int y){
        renderLeft = false;
        renderRight = false;
        renderUp = true;
        renderDown = false;
        this.x = x;
        this.y = y;
    }

    public boolean isRenderLeft() {
        return renderLeft;
    }

    public void setRenderLeft(boolean renderLeft) {
        this.renderRight = false;
        this.renderUp = false;
        this.renderDown = false;
        this.renderLeft = renderLeft;
    }

    public boolean isRenderRight() {
        return renderRight;

    }

    public void setRenderRight(boolean renderRight) {
        this.renderLeft = false;
        this.renderUp = false;
        this.renderDown = false;
        this.renderRight = renderRight;
    }

    public boolean isRenderUp() {
        return renderUp;
    }

    public void setRenderUp(boolean renderUp) {
        this.renderLeft = false;
        this.renderRight = false;
        this.renderDown = false;
        this.renderUp = renderUp;
    }

    public boolean isRenderDown() {
        return renderDown;
    }

    public void setRenderDown(boolean renderDown) {
        this.renderLeft = false;
        this.renderUp = false;
        this.renderRight = false;
        this.renderDown = renderDown;
    }

}


public class Snake {

    private Deque<Direction> movesList;

    private Deque<Boolean> boolList;
    private int cellCount;
    private int index = 0;
    private int index2 = 0;
    private int headPositionX;
    private int headPositionY;


    private boolean isMoving;

    Snake(){
        isMoving = false;
        cellCount = 10; //poczatkowa dlugosc to 10
        index = 0;
        movesList = new LinkedBlockingDeque<Direction>(200);
        boolList = new LinkedBlockingDeque<Boolean>(200);
        headPositionX = (Const.WIDTH - 200)/2;
        headPositionY = Const.HEIGHT/2;
        updateSnake(new Direction(headPositionX, headPositionY));
        updateSnake(new Boolean(false));
    }

    public void updateSnake(Direction dir) {
        dir.x = headPositionX;
        dir.y = headPositionY;
        if (index < cellCount) {
            movesList.offerFirst(dir);
            index++;
        } else {
            movesList.pollLast();
            movesList.offerFirst(dir);
        }
    }

    public void updateSnake(Boolean dir) {
        if (index2 < cellCount) {
            boolList.offerFirst(dir);
            index2++;
        } else {
            boolList.pollLast();
            boolList.offerFirst(dir);
        }
    }


    public int getCellCount(){
        return cellCount;
    }

    public void setCellCount(int cellCount) {
        this.cellCount = this.cellCount + cellCount;
    }


    public Direction getHeadDirection() {
        return movesList.peekFirst();
    }

    public void moveLeft(int offset, Direction dir){
        headPositionX = headPositionX - offset;
        isMoving = true;
        //dir.x = dir.x - offset;

    }

    public void moveRight(int offset, Direction dir){
        headPositionX = headPositionX + offset;
        isMoving = true;
        //dir.x = dir.x + offset;
    }

    public void moveUp(int offset, Direction dir){
        headPositionY = headPositionY - offset;
        isMoving = true;
        //dir.y = dir.y - offset;
    }

    public void moveDown(int offset, Direction dir){
        headPositionY = headPositionY + offset;
        isMoving = true;
        //dir.y = dir.y + offset;
    }


    public Deque<Direction> getMovesList() {
        return movesList;
    }

    public Deque<Boolean> getBoolList() {
        return boolList;
    }

    public int getHeadPositionX() {
        return headPositionX;
    }

    public void setHeadPositionX(int headPositionX) {
        this.headPositionX = headPositionX;
    }

    public int getHeadPositionY() {
        return headPositionY;
    }

    public void setHeadPositionY(int headPositionY) {
        this.headPositionY = headPositionY;
    }


    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }

}
