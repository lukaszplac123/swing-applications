package lukasz.snake.options;

import lukasz.TicTacToe.ChatTab;
import lukasz.TicTacToe.LogTab;
import lukasz.snake.SnakeGame;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Dictionary;
import java.util.Hashtable;

public class Options implements ChangeListener, ActionListener {

    private JLabel jLabel1;
    private JSlider jSlider1;
    private JLabel jLabel2;
    private JSlider jSlider2;
    private JTabbedPane jTabbedPane;

    private boolean cellHasChanged = false;


    public Options(JPanel jpnl){


        Hashtable label1 = new Hashtable();
        label1.put(new Integer(5), new JLabel("5"));
        label1.put(new Integer(10), new JLabel("10"));
        label1.put(new Integer(15), new JLabel("15"));

        Hashtable label2 = new Hashtable();
        label2.put(new Integer(1), new JLabel("1"));
        label2.put(new Integer(2), new JLabel("2"));
        label2.put(new Integer(3), new JLabel("3"));
        label2.put(new Integer(4), new JLabel("4"));
        label2.put(new Integer(5), new JLabel("5"));


        Dimension dim0 = new Dimension(200,600);
        Dimension dim1 = new Dimension(200,300);
        JPanel jpanelNorth = new JPanel(new GridLayout(6,1,1,1));
        JPanel radioButtons = new JPanel(new GridLayout(3,1,5,1));
        JPanel jpanelSouth = new JPanel(new GridLayout(1,1,1,1));
        //jpanelNorth.setSize(dim1);
        //jpanelSouth.setSize(dim1);
        jpanelSouth.setBorder(new EmptyBorder(10,5,10,5));
        radioButtons.setBorder(new CompoundBorder(new EmptyBorder(5,5,5,5), new LineBorder(Color.LIGHT_GRAY, 1)));
        jpnl.setPreferredSize(dim0);
        jLabel1 = new JLabel("Speed:");
        jLabel1.setSize(dim1);
        jSlider1 = new JSlider(SwingConstants.HORIZONTAL, 5, 15, 10 );
        jSlider1.addChangeListener(this);
        jSlider1.setMajorTickSpacing(1);
        jSlider1.setPaintTicks(true);
        jSlider1.setSnapToTicks(true);
        jSlider1.setLabelTable(label1);
        jSlider1.setPaintLabels(true);
        jSlider1.setName("Predkosc");
        jLabel2 = new JLabel("Cells:");
        jSlider2 = new JSlider(SwingConstants.HORIZONTAL, 1, 5, 1 );
        jSlider2.addChangeListener(this);
        jSlider2.setMajorTickSpacing(1);
        jSlider2.setPaintTicks(true);
        jSlider2.setSnapToTicks(true);
        jSlider2.setLabelTable(label2);
        jSlider2.setPaintLabels(true);
        jSlider2.setName("Przyrost");
        jTabbedPane = new JTabbedPane();
        jTabbedPane.addTab("Log", new LogTab());
        jTabbedPane.addTab("Chat", new ChatTab());


        jpanelNorth.add(jLabel1,0);
        jpanelNorth.add(jSlider1,1);
        jpanelNorth.add(jLabel2,2);
        jpanelNorth.add(jSlider2,3);
        jpanelSouth.add(jTabbedPane,0);
        jpnl.add(jpanelNorth, 0);
        jpnl.add(jpanelSouth, 1);
        //jpnl.validate();
        //jpnl.repaint();

    }


    public JSlider getjSlider1() {
        return jSlider1;
    }

    public JSlider getjSlider2() {
        return jSlider2;
    }

    public JTabbedPane getjTabbedPane() {
        return jTabbedPane;
    }


    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider jSlider = (JSlider) e.getSource();
        if (jSlider.getName().equals("Przyrost")) {
            cellHasChanged = true;
        }
    }

    @Override
    public synchronized void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Normal")){
            System.out.println("Click");
        }
    }

    public boolean isCellHasChanged() {
        return cellHasChanged;
    }

    public void setCellHasChanged(boolean cellHasChanged) {
        this.cellHasChanged = cellHasChanged;
    }


}
