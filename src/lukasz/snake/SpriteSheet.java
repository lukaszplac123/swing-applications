package lukasz.snake;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpriteSheet {

    private int[] spriteSheetArray;

    private BufferedImage spriteSheet;

    private final int width = 50;
    private final int height = 50;

    SpriteSheet(){
        spriteSheet =  new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        try {
            spriteSheet = ImageIO.read(new File("res/SpriteSheet.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        spriteSheetArray = new int[width * height];
        spriteSheet.getRGB(0,0,width,height,spriteSheetArray,0,width);
    }
    public int getPixel(int i){
        return spriteSheetArray[i];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}

class Sprite {

    private final int width = 10;
    private final int height = 10;
    private int index = 0;
    private int[] sprite;
    private SpriteSheet spriteSheet;


    private boolean solid;

    public static Sprite snakeCell = new Sprite(0, 0, 0);
    public static Sprite headUp = new Sprite(10, 0, 1);
    public static Sprite headDown = new Sprite(20, 0, 2);
    public static Sprite headLeft = new Sprite(30, 0, 3);
    public static Sprite headRight = new Sprite(40, 0, 4);
    public static Sprite fromDownToLeft = new Sprite(0, 10, 5);
    public static Sprite fromDownToRight = new Sprite(10, 10, 6);
    public static Sprite fromUpToRight = new Sprite(20, 10, 7);
    public static Sprite fromUpToLeft = new Sprite(30, 10, 8);
    public static Sprite apple = new Sprite(40, 10, 9);

    Sprite(int x, int y, int ind) {
        index = ind;
        spriteSheet = new SpriteSheet();
        sprite = new int[width * height];
        load(x, y);

    }

    private void load(int xx, int yy) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                sprite[i + j * (width)] = spriteSheet.getPixel((i + xx) + ((j + yy) * spriteSheet.getWidth()));
                // System.out.println("Tworzenie");
            }
        }

    }

    public int getPixel(int i) {
        return sprite[i];
    }

    public boolean isSolid() {
        return solid;
    }

    public int getIndex() {
        return index;
    }

    public static Sprite getSpriteByIndex(int i) {
        switch (i) {
            case 0:
                return snakeCell;
            case 1:
                return headUp;
            case 2:
                return headDown;
            case 3:
                return headLeft;
            case 4:
                return headRight;
            case 5:
                return fromDownToLeft;
            case 6:
                return fromDownToRight;
            case 7:
                return fromUpToRight;
            case 8:
                return fromUpToLeft;
            case 9:
                return apple;
            case 10:
                return null;
            case 11:
                return null;
            case 12:
                return null;
            case 13:
                return null;
            case 14:
                return null;
            case 15:
                return null;
            case 16:
                return null;
            case 17:
                return null;
            case 18:
                return null;
            case 19:
                return null;
            case 20:
                return null;
            case 21:
                return null;
            case 22:
                return null;
            case 23:
                return null;
            case 24:
                return null;
        }
    return null;
    }
}

