package lukasz.snake;


import java.util.Random;

public class Item {

    private int x;
    private int y;
    Random randomX = new Random();
    Random randomY = new Random();

    Item(){
        int randA = randomX.nextInt(500) + 50;
        int randB = randomY.nextInt(500) + 50;
        int moduloA = (randA)%10;
        int moduloB = (randB)%10;

        if (moduloA < 5){
            x = randA - moduloA;
        } else x = randA + (10 - moduloA);

        if (moduloB < 5){
            y = randB - moduloB;
        } else y = randB + (10 - moduloB);

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
