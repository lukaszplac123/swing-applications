package lukasz.snake;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class Keyboard implements KeyListener {

    private int keyPressed;


    private boolean upCommand = false;
    private boolean downCommand = false;
    private boolean leftCommand = false;
    private boolean rightCommand = false;


    public void update(){
        if ((keyPressed == KeyEvent.VK_LEFT || keyPressed == KeyEvent.VK_A) &
           !(rightCommand)) leftCommand = true;
        if ((keyPressed == KeyEvent.VK_RIGHT || keyPressed == KeyEvent.VK_D) &
           !(leftCommand)) rightCommand = true;
        if ((keyPressed == KeyEvent.VK_UP || keyPressed == KeyEvent.VK_W) &
           !(downCommand)) upCommand = true;
        if ((keyPressed == KeyEvent.VK_DOWN || keyPressed == KeyEvent.VK_S) &
                !(upCommand)) downCommand = true;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        keyPressed = e.getKeyCode();
        if (((keyPressed == KeyEvent.VK_LEFT || keyPressed == KeyEvent.VK_A)) & (rightCommand)){
            upCommand = false;
            downCommand = false;
            leftCommand = false;
            return;
        } else if ((keyPressed == KeyEvent.VK_RIGHT || keyPressed == KeyEvent.VK_D) & (leftCommand)){
            upCommand = false;
            downCommand = false;
            rightCommand = false;
            return;
        } else if ((keyPressed == KeyEvent.VK_UP || keyPressed == KeyEvent.VK_W) & (downCommand)){
            upCommand = false;
            rightCommand = false;
            leftCommand = false;
            return;
        } else if ((keyPressed == KeyEvent.VK_DOWN || keyPressed == KeyEvent.VK_S) & (upCommand)){
            downCommand = false;
            rightCommand = false;
            leftCommand = false;
            return;
        } else{
            downCommand = false;
            rightCommand = false;
            leftCommand = false;
            upCommand = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    public boolean isUpCommand() {
        return upCommand;
    }

    public boolean isDownCommand() {
        return downCommand;
    }

    public boolean isLeftCommand() {
        return leftCommand;
    }

    public boolean isRightCommand() {
        return rightCommand;
    }


}
