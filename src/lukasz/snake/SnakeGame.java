package lukasz.snake;

import lukasz.TicTacToe.LogTab;
import lukasz.helpers.Const;
import lukasz.snake.options.Options;
import sun.rmi.runtime.Log;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import java.io.IOException;
import java.util.Iterator;

public class SnakeGame implements Runnable, WindowListener, KeyListener {

    public static int points;

    private BufferedImage image;
    private int pixels[];
    private int width;
    public int height;
    public int canvasWidth;
    private Dimension frameDim;
    private Dimension canvasDim;

    //private Dimension panelDim;
    private Thread thread;
    private boolean isGame;
    private JFrame frame;
    private Screen screen;
    private JPanel panel;
    private Keyboard key;
    private String title;
    private Direction direction;
    private Options opt;
    private boolean isCloseWindow;
    private boolean isQuitCommand;



    public SnakeGame(){
        points = 0;
        thread = new Thread(this);
        key = new Keyboard();
        title = "Snake2D by Lukasz Plac";
        this.width = Const.WIDTH; //1000
        this.height = Const.HEIGHT; //800
        canvasWidth = width - 200; //800
        pixels = new int[(canvasWidth)*(height)];
        image = new BufferedImage(canvasWidth,canvasWidth, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
        frameDim = new Dimension(width, height);
        canvasDim = new Dimension(width-200, height);
        //panelDim = new Dimension(300, height);
        frame = new JFrame();
        frame.setMinimumSize(frameDim);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        screen = (Screen) new Screen(canvasDim);
        screen.addKeyListener(key);
        screen.addKeyListener(this);
        panel = new JPanel(new GridLayout(2,1,1,1));
        //panel.setSize(panelDim);
        panel.setBackground(Const.DEFAULT_BACKGROUND);
        isGame = false;
        isCloseWindow = false;
        isQuitCommand = false;
        opt = new Options(panel);
        start();
    }

    public void start(){
        isGame = true;
        thread.start();
    }

    @Override
    public void run() {
        frame.add(panel);
        frame.add(screen, BorderLayout.EAST);
        frame.pack();
        frame.setVisible(true);
        frame.addWindowListener(this);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        long savedTime = System.nanoTime();
        int fps = 0;
        int ups = 0;
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        //final double ns = 1000000000.0 / 10;
        double delta = 0;
        //Tak dla pamieci. FPS bez BufferStrategy wynosi ok 450
        while (isGame) {
            double ns = 1000000000.0 / opt.getjSlider1().getValue();
            if (opt.isCellHasChanged()) {
                screen.setSnakeGrowth(opt.getjSlider2().getValue());
                opt.setCellHasChanged(false);
                //System.out.println(opt.getjSlider2().getValue());
            }
            screen.requestFocus();
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            //try {
//                thread.sleep(2000);
//                System.out.println("##################");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            lastTime = now;
            while (delta >= 1) {
                update();
                ups++;
                delta--;
            }
            render();
            fps++;
            //obliczenia fps i ups i wyswietlanie wynikow co sekunde
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frame.setTitle(title + " | " + ups + " ups, " + fps + " fps");
                ups = 0;
                fps = 0;
            }
            LogTab.setText();
        }
        if ((!isCloseWindow) & (!isQuitCommand)) {
            LogTab.log.append("Press Q or close the\nwindow to Quit");
            LogTab.setText();
        }
    }


    public void stop(Thread thread){
        isGame = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        frame.setVisible(false);
    }
    //boolean bool = false;
    public synchronized void update(){

        direction = new Direction((Const.WIDTH - 200)/2,Const.HEIGHT/2);
        key.update();
        if (key.isLeftCommand()) {
            direction.setRenderLeft(true);
            screen.getSnake().moveLeft(10, direction);
            //bool = true;
        }
        if (key.isRightCommand()) {
            direction.setRenderRight(true);
            screen.getSnake().moveRight(10, direction);
            //bool = false;
        }
        if (key.isUpCommand()) {
            direction.setRenderUp(true);
            screen.getSnake().moveUp(10, direction);
        }
        if (key.isDownCommand()) {
            direction.setRenderDown(true);
            screen.getSnake().moveDown(10, direction);
        }
        //if (!screen.getSnake().isMoving()) {
           // direction.y = direction.y +10;
            screen.getSnake().updateSnake(direction);
        // }
//        Iterator<Boolean> iter = screen.getSnake().getBoolList().iterator();
//        while (iter.hasNext()){
//            System.out.print(iter.next());
//        }
//        System.out.println("-------------");


//        System.out.println("----");
//        System.out.println(screen.getSnake().getMovesList());
//        System.out.flush();

    }

    public synchronized void render(){

        BufferStrategy bs = screen.getBufferStrategy();
        if (bs == null){
            screen.createBufferStrategy(3);
            return;
        }

        //screen.clear();
        if (!screen.render()){
            isGame = false;
        }
            for (int i = 0; i < pixels.length; i++) {
                pixels[i] = screen.getPixel(i);
            }
        Graphics g =  bs.getDrawGraphics();
        g.drawImage(image, 0 , 0, canvasWidth, height, null);
        g.setColor(Color.CYAN);
        g.drawString("Points:"+ points,5,20);
        g.dispose();
        bs.show();
//        if (!screen.render()) {
//            isGame = false;
//        }
//        //screen.clear();
//        Graphics g = screen.getGraphics();
//        for (int i = 0; i < pixels.length; i++) {
//            pixels[i] = screen.getPixel(i);
//        }
//        g.drawImage(image, 0, 0, canvasWidth, height, null);
//        g.dispose();
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        isCloseWindow = true;
        stop(thread);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyPressed = e.getKeyCode();
        if (keyPressed == KeyEvent.VK_Q) {
            isQuitCommand = true;
            stop(thread);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public Thread getThread() {
        return thread;
    }
}
