package lukasz.snake;

import lukasz.TicTacToe.LogTab;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class Screen extends Canvas {



    private BufferedImage borderMap;
    private int pixels[];
    private int map[];
    private boolean logicScreen[];
    private int width, height;
    private int snakeGrowth;



    private Snake snake;
    private Item apple;


    public Screen(Dimension dim){
        setSize(dim);
        snakeGrowth = 1;
        width = (int) dim.getWidth();
        height = (int) dim.getHeight();
        try {
            borderMap = ImageIO.read(new File("res/borderMap.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        map = new int[width * height];
        pixels = new int[width * height];
        logicScreen = new boolean[width * height];
        borderMap.getRGB(0,0,width,height,map,0,width);
        snake = new Snake();
        apple = new Item();
    }



    public int getPixel (int i){
        return pixels[i];
    }

    public void clear(){
        for (int y = 0; y < height; y++ ){
            for (int x = 0; x < width; x++){
                pixels[y + x * width] = map[y + x * width];
            }
        }

    }
    //long licznik = 0;
    public boolean render() {

        Iterator<Direction> iter = snake.getMovesList().iterator();
        int index = 0;
        int xh = 0;
        int yh = 0;
        boolean corruption = false;


        //RENDEROWANIE MAPY
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels[y + x * width] = map[y + x * width];
            }
        }

        //RENDEROWANIE WEZA
        while (iter.hasNext()){
            Direction d = iter.next();
            //zapamietana pozycja glowy
            if (index == 0) {
                xh = d.x;
                yh = d.y;
            }
            int pixel = 0;
            for (int y = 0; y < 10; y++) {
                for (int x = 0; x < 10 ; x++) {
                     if (d.isRenderUp()){
                         if (index == 0){
                             if (checkState(x + d.x,y + d.y, corruption) == false) corruption = true;
                             pixel = Sprite.headUp.getPixel(x + y  * 10);
                         } else {
                             if (snake.isMoving()) {
                                 if (checkState(x + d.x, y + d.y, xh, yh) == false) corruption = true;
                             }
                             pixel = Sprite.snakeCell.getPixel(x + y* 10);
                         }
                     }
                     if (d.isRenderLeft()){
                         if (index == 0){
                             if (checkState(x + d.x,y + d.y, corruption) == false) corruption = true;
                             pixel = Sprite.headLeft.getPixel(x + y  * 10);
                         } else {
                             if (snake.isMoving()) {
                                 if (checkState(x + d.x, y + d.y, xh, yh) == false) corruption = true;
                             }
                             pixel = Sprite.snakeCell.getPixel(x + y* 10);
                         }
                     }

                    if (d.isRenderRight()){
                        if (index == 0){
                            if (checkState(x + d.x,y + d.y, corruption) == false) corruption = true;
                            pixel = Sprite.headRight.getPixel(x + y  * 10);
                        } else {
                            if (snake.isMoving()) {
                                if (checkState(x + d.x, y + d.y, xh, yh) == false) corruption = true;
                            }
                            pixel = Sprite.snakeCell.getPixel(x + y* 10);
                        }
                    }

                    if (d.isRenderDown()){
                        if (index == 0){
                            if (checkState(x + d.x,y + d.y, corruption) == false) corruption = true;
                            pixel = Sprite.headDown.getPixel(x + y  * 10);
                        } else {
                            if (snake.isMoving()) {
                                if (checkState(x + d.x, y + d.y, xh, yh) == false) corruption = true;
                            }
                            pixel = Sprite.snakeCell.getPixel(x + y * 10);
                        }
                    }

                    pixels [((d.x + x) + ((d.y + y) * width))] = pixel;

                }
            }
            index++;
        }

        //RENDEROWANIE JABLEK
        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10 ; x++) {
                pixels[(apple.getX() + x) + ((apple.getY() + y)) * width] = Sprite.apple.getPixel(x + y  * 10);
            }
        }
    if (!corruption){
        return true;
        }else return false;
    }



    public Snake getSnake() {
        return snake;
    }

    private boolean checkState(int x, int y, boolean corruption){
        if ((x == apple.getX()) & y == apple.getY()) {
            LogTab.log.append("Mmmhhhhhhh....\n");
            //LogTab.setText();
            SnakeGame.points += 10;
            apple = new Item();
            snake.setCellCount(snakeGrowth);  //dodaje wiecej komorek po zjedzeniu jablka
        } else if ((x == 1) || (x == (width - 1)) || ((y == 1) || (y == (height - 1)))) {
            if (!corruption) {
                LogTab.log.append("Outside plying\nbounds!\n");
                //LogTab.setText();
            }
            return false;
        }
    return true;
    }

    private boolean checkState(int x, int y, int xh, int yh){
        if ((x == xh) & (y == yh)){
            LogTab.log.append("Do not eat\nyour tail!\n");
            //LogTab.setText();
            return false;
        }
        return true;
    }

    public void setSnakeGrowth(int snakeGrowth) {
        this.snakeGrowth = snakeGrowth;
    }




}
